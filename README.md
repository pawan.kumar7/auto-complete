# auto-complete

- Set of pre-canned questions as per your requirement and create a JSON file.
- Upload the JSON file in any publically accessible URL such as Github. 
- Add the JSON URL in the query_autocomplete_url parameter either in the web channel configuration or as a param in the web channel URL. 
- You can now test your web channel. As you type the text, the auto-complete list is displayed in the agent.
